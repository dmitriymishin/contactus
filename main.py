# -*- coding: utf-8 -*-
import json
from multiprocessing.pool import ThreadPool
import sys
import heapq
import logging
from operator import itemgetter
import os
import sqlite3
import string
import urlparse
from PyQt4 import QtGui, QtCore, uic
from lxml.html import CheckboxValues, MultipleSelectOptions
from test_grabber import spider, send_data


SETTINGS_FILEPATH = os.path.join(os.getcwdu(), "fields.json")
DATABASE_FILEPATH = os.path.join(os.getcwdu(), "database.sqlite3")

# states
IDLE = 1
SETTINGS = 2
RUNNING = 3
DB_UPDATE = 4
# end states

logging.basicConfig(level=logging.INFO)

# collect urls from txt files in /files/*.txt
# one url per line
def get_urls():
    cwd = os.getcwdu()
    files_dir = os.path.join(cwd, "files")
    file_list = filter(lambda name: name.endswith(".txt"), os.listdir(files_dir))
    file_list = map(lambda name: os.path.join(files_dir, name), file_list)
    result = []

    for f_path in file_list:
        with open(f_path) as fp:
            result.append(map(string.strip, fp.readlines()))
    return (urlparse.urljoin(i, "/") for i in heapq.merge(*result))

class Database(object):
    def __init__(self, name='sqlite.db', data=None):
        self.conn = sqlite3.connect(name)
        self.cur = self.conn.cursor()

        if data is not None:
            map(self.add_url, data)

    def init_db(self):
        logging.debug("init db")
        self.cur.execute("""
        CREATE TABLE IF NOT EXISTS links
        (id INTEGER PRIMARY KEY AUTOINCREMENT, domain VARCHAR(256), url VARCHAR(256) UNIQUE, done BOOL DEFAULT 0, result BOOL default 0)
        """)
        self.conn.commit()

    def close(self):
        self.cur.close()
        self.conn.close()

    def done_url(self, url, result=None):
        self.cur.execute("""
        UPDATE links SET done = 1, result = {result} WHERE url = '{url}';
        """.format(url=url, result=int(bool(result))))
        self.conn.commit()
        logging.info("Done url %s" % url)

    def add_url(self, url):
        logging.debug("Add url %s" % url)
        if not urlparse.urlparse(url).netloc:
            url = "http://" + url
        try:
            self.cur.execute("""
            INSERT INTO links (domain, url)
            values('{domain}', '{url}')
            """.format(domain=urlparse.urlparse(url).netloc, url=url))
            self.conn.commit()
            logging.info("Url added to the database %s" % url)
        except sqlite3.IntegrityError as e:
            logging.warning("Not unique url was skipped: %s" % url)
        # self.cur.execute("SELECT id FROM links where url='{url}';".format(url=url))
        # return self.cur.fetchone()[0]

    def db_chunk(self):
        self.cur.execute("SELECT url FROM links WHERE done = 0 ORDER BY id ASC;")
        return map(itemgetter(0), self.cur.fetchmany(5))

    def exists(self):
        self.cur.execute("""SELECT count(id) FROM links WHERE done = 0;""")
        return bool(self.cur.fetchone()[0])

    def get_info(self):
        self.cur.execute("""
SELECT * FROM
(select count(id) as total from links),
   (select count(id) as done from links where done = 1),
   (select count(id) as result from links where result = 1),
   (select (  ((select count(id) from links where done= 1) * 1.0) /  ((select count(id) as total from links)*1.0)  ) * 100.0 as done_p),
   (select (  ((select count(id) from links where result= 1) * 1.0) /  ((select count(id) from links where done= 1)*1.0)  ) * 100.0 as cover_p);
        """)
        return self.cur.fetchone()


class Settings(QtGui.QDialog):
    def __init__(self, *args, **kwargs):
        super(Settings, self).__init__(*args, **kwargs)
        uic.loadUi("SettingsScreen.ui", self)
        self.load_data()

    def load_data(self):
        if not os.path.exists(SETTINGS_FILEPATH):
            data = {}
        else:
            with open(SETTINGS_FILEPATH) as fp:
                data = json.load(fp)
        self.first_name.setText(data.get("first_name", ""))
        self.last_name.setText(data.get("last_name", ""))
        self.email.setText(data.get("email", ""))
        self.phone.setText(data.get("phone", ""))
        self.website.setText(data.get("website", ""))
        self.company.setText(data.get("company", ""))
        self.subject.setText(data.get("subject", ""))
        self.message.setPlainText(data.get("message", ""))

        QtCore.QObject.connect(self, QtCore.SIGNAL("finished (int)"), self, QtCore.SLOT("dialogIsFinished(int)"))

    @QtCore.pyqtSlot(int)
    def dialogIsFinished(self, state):
        if state == QtGui.QDialog.Accepted:
            data = {}
            data['first_name'] = self.first_name.text().toUtf8().data()
            data['last_name'] = self.last_name.text().toUtf8().data()
            data['email'] = self.email.text().toUtf8().data()
            data['phone'] = self.phone.text().toUtf8().data()
            data['website'] = self.website.text().toUtf8().data()
            data['company'] = self.company.text().toUtf8().data()
            data['subject'] = self.subject.text().toUtf8().data()
            data['message'] = self.message.toPlainText().toUtf8().data()
            with open(SETTINGS_FILEPATH, "w") as fp:
                json.dump(data, fp)
            logging.info("Changes saved!")
            for k, v in data.iteritems():
                logging.info(u"{0} = {1}".format(k, v.decode("utf-8")))
        else:
            logging.warning("Rejected! Changes won't be saved!")


class MainWindow(QtGui.QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        uic.loadUi("MainWindow.ui", self)
        self.state = IDLE

        self.setWindowTitle(u"Contact Us Form")
        self.resize(400, 400)
        self.setup_gui()

    def setup_gui(self):
        self.connect(self.actionSettings, QtCore.SIGNAL("triggered()"), self.show_settings)
        self.connect(self.start_button, QtCore.SIGNAL("clicked()"), self.start)
        self.connect(self.stop_button, QtCore.SIGNAL("clicked()"), self.stop)
        self.connect(self.init_button, QtCore.SIGNAL("clicked()"), self.init_database)

        def ready_check():

            if not os.path.exists(SETTINGS_FILEPATH):
                self.show_settings()
            elif not os.path.exists(DATABASE_FILEPATH):
                self.start_button.setDisabled(True)
                self.stop_button.setDisabled(True)
                self.init_button.setEnabled(True)
            elif self.state == DB_UPDATE:
                self.start_button.setDisabled(True)
                self.stop_button.setDisabled(True)
                self.init_button.setDisabled(True)
            elif self.state == RUNNING:
                self.start_button.setDisabled(True)
                self.stop_button.setEnabled(True)
                self.init_button.setDisabled(True)
            elif self.state == IDLE:
                self.start_button.setEnabled(True)
                self.stop_button.setDisabled(True)
                self.init_button.setEnabled(True)

        self.__ready_timer = QtCore.QTimer(self)
        self.connect(self.__ready_timer, QtCore.SIGNAL("timeout()"), ready_check)
        self.__ready_timer.setInterval(20)
        self.__ready_timer.start()


        def update_info():
            db = Database(name=DATABASE_FILEPATH)
            total, done, result, done_p, cover_p = db.get_info()
            db.close()

            self.done_p.setValue(done_p if done_p is not None else 0)
            self.cover_p.setValue(cover_p if cover_p is not None else 0)
            self.info_total.display(total)
            self.info_done.display(done)
            self.info_result.display(result)


        self.update_info_timer = QtCore.QTimer(self)
        self.connect(self.update_info_timer, QtCore.SIGNAL("timeout()"), update_info)
        self.update_info_timer.setInterval(100)
        self.update_info_timer.start()


    def init_database(self):
        def add_url():
            db = Database(name=DATABASE_FILEPATH)
            try:
                url = self.__urls.next()
                db.add_url(url)
            except StopIteration as e:
                self.__timer.stop()
                self.state = IDLE
                logging.info("Database is up-to-date")
            finally:
                db.close()
        db = Database(name=DATABASE_FILEPATH)
        db.init_db()
        db.close()
        self.__timer = QtCore.QTimer(self)
        self.connect(self.__timer, QtCore.SIGNAL("timeout()"), add_url)
        self.__urls = get_urls()
        self.__timer.setInterval(10)
        self.__timer.start()
        self.state = DB_UPDATE

    def show_settings(self):
        d = Settings(self)
        d.exec_()

    def start(self):
        self.state = RUNNING

        self.thread = Worker(self)
        self.thread.start()


    def stop(self):
        self.state = IDLE


class JsonSerializer(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, (CheckboxValues, MultipleSelectOptions,)):
            return list(o)
        return super(JsonSerializer, self).default(o)


class Worker(QtCore.QThread):
    log = QtCore.pyqtSignal(basestring)

    def __init__(self, *args, **kwargs):
        super(Worker, self).__init__(*args, **kwargs)

    def run(self):
        db = Database(name=DATABASE_FILEPATH)
        while self.parent().state == RUNNING and db.exists():
            pool = ThreadPool(processes=5)
            for (url, forms) in pool.imap(spider, db.db_chunk()):
                try:
                    data_sended = send_data(forms)
                except Exception as e:
                    with open("failures.txt", 'a') as fp:
                        print >> fp, "Send data Failure: [{0}] {1} {2}".format(e, url, json.dumps(forms))
                with open("results.txt", "a") as fp:
                    try:
                        print>> fp, url, "\t", json.dumps(forms), "\t", json.dumps(data_sended, cls=JsonSerializer)
                    except Exception as e:
                        with open("failures.txt", "a") as fp:
                            print >> fp, "Fail dump data: {0} - {1}".format(e, json.dumps(forms))
                            print data_sended
                logging.info("{url} - {forms}".format(url=url, forms=json.dumps(forms)))
                db.done_url(url, forms)
        print "STOPPED. NOW YOU CAN CLOSE APPLICATION IF IT NEED."




if __name__ == "__main__":
    app = QtGui.QApplication([])
    wnd = MainWindow()
    wnd.show()
    sys.exit(app.exec_())