# -*- coding: utf-8 -*-
import json
import random
import re
import string
import urlparse
from multiprocessing.pool import ThreadPool
import logging
from lxml.html import RadioGroup, CheckboxGroup
import grab
import grab.error


class MultiRegexpr(object):
    def __init__(self, regexpr_list):
        self.list = regexpr_list

    def match(self, string):
        for rx in self.list:
            if rx.match(string):
                return rx.match(string)
        return None

MESSAGE = re.compile(r'^(?:.+)?(comment(?:s)?|message|description|body|tellus)(?:.+)?$')
FIELDS = dict(
    input=dict(
        first_name=MultiRegexpr([re.compile(r'^(?:.+)?(f(?:irst|ull(.+)?)?(name|author)(?!url|email))(?:.+)?$'),
                                 re.compile(r'^(?:.+)?name(?:.+)?$')]),
        last_name=re.compile(r'^(?:.+)?((?<!ful)l(?:ast(?:.+)?)?name)(?:.+)?$'),
        subject=re.compile(r'^(?:.+)?(subject|title)(?:.+)?$'),
        email=re.compile(r'^(?:.+)?(email(?!_body))(?:.+)?$'),
        company=re.compile(r'^(?:.+)?(company(?!url))(?:.+)?$'),
        phone=re.compile(r'^(?:.+)?(phone|tel)(?:.+)?$'),
        website=re.compile(r'^(?:.+)?(web|url|website)(?:.+)?$'),
        message=MESSAGE,
    ),
    textarea=dict(
        message=MESSAGE
    )
)


# check it it's not a file link
NOT_FILE = lambda url: not re.match(r'(.+)?\.(jpg|gif|png|pdf|xml|tiff|ico|txt|eps|mp3|flv|mp4|mkv)$', url)
# skip /blog /forum / upload* urls to decrease working time and network loading
VALID_LINK = lambda url: not re.match(r'^\/(blog|forum|upload(s)?)(.+)?$',
                                      urlparse.urlparse(url).path.lower()) and not re.match(r'^.+(\#.+)$',
                                                                                            url) and url.strip() != "javascript:void(0)"
# pattern for form names. if name is empty - form valid.
VALID_NAME = lambda name: not re.match(
    r'(.+)?(login|signup|signin|find|search|forgot|lost|register|subscribe|language|newsletter|reset)(.+)?',
    name.lower())


# check that it's not a big corporation
def not_google(url):
    regex_part = "|".join([u"facebook", u'google', u'yandex', u'mail', 'rambler',
                           u'twitter', u'vk', u'linkedin', u'linked', u"tumblr",
                           u"instagram", "goo"])
    pattern = re.compile(r'^(.+\.)?(%s)\.[a-zA-Z0-9]{2,}$' % regex_part)
    return not bool(pattern.match(urlparse.urlparse(url).netloc or urlparse.urlparse(url).path))


# try to apply pattern to form inputs and check if it can be contact-us form.
def check_forms(forms_list):
    if not forms_list:
        return

    result = {}
    for number, form in enumerate(forms_list, start=0):
        if len(dict(form.inputs).keys()) < 2:
            continue
        success = 0

        if not all(map(VALID_NAME, [form.attrib.get("name", ""), form.attrib.get("id", "")])):
            continue

        for k, v in dict(form.inputs).iteritems():
            if "name" not in getattr(v, "attrib", {}):
                continue

            if isinstance(v, (RadioGroup, CheckboxGroup,)) or v.tag == "select":
                continue
            if v.attrib.get("type", None) in ("hidden", "submit"):
                continue
            for role, pattern in FIELDS.get(v.tag, {}).iteritems():
                if pattern.match(v.attrib.get("name", "").lower()):
                    success += 1

        if success >= 2:
            data = dict(form.attrib)
            data["number"] = number
            result[success] = dict(filter(lambda item: item[0] in ["number", "id", "name"], data.items()))
    if result:
        result = result[max(result.keys())]
        return result


# Web spider.
def spider(main_url):
    def async_loader(url):
        g = grab.Grab()
        try:
            g.go(url)
        except (grab.error.GrabTimeoutError,
                grab.error.GrabConnectionError,
                grab.error.GrabNetworkError,
                grab.error.GrabTooManyRedirectsError) as e:
            logging.warning(e)
            return None, url
        if g.response.code >= 400:
            return None, url
        try:
            c_type = g.response.headers['Content-Type'].split(";")[0]
        except AttributeError as e:
            logging.warning(e)
            return None, url
        if c_type not in ["text/html"]:
            return None, url
        return g, url

    g = grab.Grab()
    open_list = [main_url]
    close_list = []

    result = {}
    pool = ThreadPool(processes=2)
    n = 0
    while open_list and n < 3:
        n += 1
        current_list = open_list[:]
        open_list = []
        for g, url in pool.imap(async_loader, current_list):
            if g is None:
                close_list.append(url)
                continue
            links = map(lambda a: a.attrib.get("href", "#").strip(), g.css_list("a"))
            links = filter(lambda x: x.strip() not in [u'#', u'javascript:void(0)', u'javascript:;'], links)
            tmp_links = links[:]
            links = []
            for link in tmp_links:
                try:
                    links.append(urlparse.urljoin(g.response.url, link))
                except ValueError as e:
                    pass
            links = filter(lambda link: all([g.response.url_details().netloc == urlparse.urlparse(link).netloc,
                                             link not in close_list,
                                             not_google(link),
                                             NOT_FILE(link),
                                             VALID_LINK(link)]), links)
            form = check_forms(g.css_list("form"))
            if form is not None:
                result[url] = form

            close_list.append(url)
            for link in links:
                if link not in close_list and link not in open_list and not_google(link):
                    if re.match(r'\/(.+)?(about|contact|feedback|help|email|kontakt|company)(.+)?',
                                urlparse.urlparse(link).path.lower()):
                        open_list.append(link)

    pool.close()
    result = {json.dumps(form): link for link, form in result.iteritems()}
    result = {link: json.loads(form) for form, link in result.iteritems()}
    return main_url, result


def send_data(forms):
    if not forms:
        return {}
    result = []
    for url, form_data in forms.items():
        g = grab.Grab()
        g.go(url)

        for identificator in ['name', 'id', 'number']:
            if identificator in form_data:
                try:
                    g.choose_form(**{identificator: form_data[identificator]})
                    break
                except grab.error.DataNotFound as e:
                    print e

        for name, element in dict(g.form.inputs).items():
            if isinstance(element, (RadioGroup,)):
                value = element[0].attrib['value']
                g.set_input(name, value)
                continue
            if isinstance(element, (CheckboxGroup,)):
                value = element[0].attrib['value']
                g.set_input(name, [value])
                continue

            if element.tag == 'select':
                try:
                    value = random.choice(element.value_options[1:])
                    g.set_input(name, [value] if element.attrib.get("multiple", None) == "multiple" else value)
                except IndexError as e:
                    print "Select", e, element.value_options
                continue

            for role, regexpr in FIELDS.get(element.tag, {}).items():
                if regexpr.match(name.lower()):
                    g.set_input(name, json.load(open('fields.json'))[role])
                elif element.attrib.get('type', None) not in ['hidden', 'submit']:
                    if not any([rx.match(name.lower()) for role, rx in FIELDS.get(element.tag, {}).items()]):
                        g.set_input(name, ''.join(map(lambda x: random.choice(string.ascii_letters),
                                                      xrange(random.randint(5, 16)))))
        try:
            result.append(dict(g.form.fields))
            g.submit()
        except Exception as e:
            with open("failures.txt", 'w') as fp:
                print >> fp, "Fail submit form {0} - {1}".format(url, json.dumps(form_data))

    return result

